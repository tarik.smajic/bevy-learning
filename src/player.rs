use crate::vector::*;
use bevy::{prelude::*, sprite::*};

#[derive(Component, Clone)]
pub struct HexComponent {
    pub length: f32,
    pub height: f32,
    pub width: f32,
}

#[derive(Component)]
pub struct Speed(Vector);

impl HexComponent {
    pub fn new(len: f32) -> HexComponent {
        // Flat top
        HexComponent {
            length: len,
            height: f32::sqrt(3.) * len,
            width: 2. * len,
        }
    }
}

pub struct PlayerPlugin;
impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_hexagon).add_systems(
            Update,
            (
                handle_arrow_control,
                move_according_to_speed,
                slow_down_speed,
            ),
        );
    }
}

fn move_according_to_speed(
    mut hex_query: Query<(&mut Transform, &mut Speed), With<HexComponent>>,
    camera: Query<&OrthographicProjection>,
) {
    let (mut trans, mut speed) = hex_query.single_mut();
    trans.translation.x += speed.0.x;
    trans.translation.y += speed.0.y;

    let camera = camera.single();
    let (xmax, ymax) = (camera.area.max.x, camera.area.max.y);
    let (xmin, ymin) = (camera.area.min.x, camera.area.min.y);

    if xmax < trans.translation.x || xmin > trans.translation.x {
        speed.0.inverse_x();
    }

    if ymax < trans.translation.y || ymin > trans.translation.y {
        speed.0.inverse_y();
    }
}

fn slow_down_speed(mut speed_query: Query<&mut Speed, With<HexComponent>>) {
    let mut speed = speed_query.single_mut();
    if speed.0.x < 0. {
        speed.0.x += 0.1;
    } else if speed.0.x > 0. {
        speed.0.x -= 0.1;
    }

    if speed.0.y < 0. {
        speed.0.y += 0.1;
    } else if speed.0.y > 0. {
        speed.0.y -= 0.1;
    }
}

fn handle_arrow_control(
    mut query_hex: Query<&mut Speed, With<HexComponent>>,
    keyboard: Res<Input<KeyCode>>,
) {
    let mut speed = query_hex.single_mut();

    let iteration = 0.2;
    if keyboard.pressed(KeyCode::Left) {
        speed.0.x -= iteration;
    }
    if keyboard.pressed(KeyCode::Right) {
        speed.0.x += iteration;
    }
    if keyboard.pressed(KeyCode::Up) {
        speed.0.y += iteration;
    }
    if keyboard.pressed(KeyCode::Down) {
        speed.0.y -= iteration;
    }
}

fn spawn_hexagon(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let hex = HexComponent::new(40.);
    commands.spawn((
        hex.clone(),
        Speed(Vector { x: 0., y: 0. }),
        MaterialMesh2dBundle {
            mesh: meshes
                .add(shape::RegularPolygon::new(hex.length, 6).into())
                .into(),
            material: materials.add(Color::BLUE.into()).into(),
            transform: Transform::from_xyz(0., 0., 0.),
            ..default()
        },
    ));
}
