pub struct Vector {
    pub x: f32,
    pub y: f32,
}

impl Vector {
    pub fn add(&mut self, operand: &Vector) {
        self.x += operand.x;
        self.y += operand.y;
    }

    pub fn scale(&mut self, scallar: f32) {
        self.x *= scallar;
        self.y *= scallar;
    }

    pub fn reverse(&mut self) {
        self.x = -self.x;
        self.y = -self.y;
    }

    pub fn inverse_x(&mut self) {
        self.x = -self.x;
    }

    pub fn inverse_y(&mut self) {
        self.y = -self.y;
    }
}
