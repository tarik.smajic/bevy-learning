use std::f32::consts::PI;

use bevy::window::WindowResolution;
use bevy::{prelude::*, sprite::collide_aabb::collide, sprite::MaterialMesh2dBundle};
use rand::Rng;
mod player;
mod vector;

#[derive(Component)]
struct Circle;

#[derive(Resource)]
struct SpawnTimer(Timer);

#[derive(Resource)]
struct CircleMoveTimer(Timer);

#[derive(Resource)]
struct TriangleSpawnTimer(Timer);

#[derive(Component)]
struct Triangle;

#[derive(Resource)]
struct Score {
    value: i32,
}

#[derive(Component)]
struct Scoreboard;

// Challange
//
// TODO
// * Implement triangles fireing bullets.
// * Triangles have fuel. When they go out, they are collectable.
// * Implement player ability shield that bursts a half circle

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title: "This is my game".to_string(),
                    resolution: WindowResolution::new(1024., 720.),
                    ..default()
                }),
                ..Default::default()
            }),
            player::PlayerPlugin,
        ))
        .add_systems(Startup, (spawn_camera, setup_score))
        .insert_resource(Score { value: 0 })
        .add_systems(
            Update,
            (
                spawn_circles,
                spawn_triangles,
                advance_timer,
                move_circles,
                move_triangles,
                calcualte_colision,
                update_score,
            ),
        )
        .insert_resource(SpawnTimer(Timer::from_seconds(2., TimerMode::Repeating)))
        .insert_resource(TriangleSpawnTimer(Timer::from_seconds(
            5.,
            TimerMode::Repeating,
        )))
        .insert_resource(CircleMoveTimer(Timer::from_seconds(
            0.02,
            TimerMode::Repeating,
        )))
        .run();
}

// Score UI
fn setup_score(mut commands: Commands, asset_server: Res<AssetServer>) {
    // Score text
    commands.spawn((
        Scoreboard,
        TextBundle {
            text: Text::from_section(
                "Score: 0", // Initial text
                TextStyle {
                    font: asset_server.load("fonts/BigBlueTermFont.ttf"), // Add your font path
                    font_size: 35.0,
                    color: Color::WHITE,
                },
            ),
            ..Default::default()
        },
    ));
}

fn update_score(mut scoreboard: Query<&mut Text, With<Scoreboard>>, score: Res<Score>) {
    let mut score_text = scoreboard.single_mut();
    score_text.sections[0].value = format!("Score {}", score.value);
}

// ==================

fn calcualte_colision(
    mut commands: Commands,
    query_circles: Query<(Entity, &Transform), With<Circle>>,
    query_player: Query<(&player::HexComponent, &Transform), With<player::HexComponent>>,
    mut score: ResMut<Score>,
) {
    let (hex, hex_transform) = query_player.single();
    for (entity, &circle) in &query_circles {
        if collide(
            hex_transform.translation,
            Vec2::new(hex.width, hex.height),
            circle.translation,
            Vec2::new(55., 55.),
        )
        .is_some()
        {
            commands.entity(entity).despawn();
            score.value += 1;
        }
    }
}

fn move_circles(
    mut commands: Commands,
    mut query_circles: Query<(Entity, &mut Transform), With<Circle>>,
    timer: Res<CircleMoveTimer>,
    camera: Query<&OrthographicProjection>,
) {
    let camera = camera.single();
    let min_x = camera.area.min.x;
    if timer.0.just_finished() {
        for (entity, mut circle) in &mut query_circles {
            circle.translation.x -= 1.;
            if circle.translation.x < min_x {
                commands.entity(entity).despawn();
            }
        }
    }
}

fn move_triangles(
    mut commands: Commands,
    query_player: Query<&Transform, (With<player::HexComponent>, Without<Triangle>)>,
    mut query_triangles: Query<
        (Entity, &mut Transform),
        (With<Triangle>, Without<player::HexComponent>),
    >,
    timer: Res<CircleMoveTimer>,
    camera: Query<&OrthographicProjection>,
) {
    if timer.0.just_finished() {
        let camera = camera.single();
        let min_x = camera.area.min.x;
        let min_y = camera.area.min.y;
        let max_x = camera.area.max.x;
        let max_y = camera.area.max.y;
        let tranform_player = query_player.single();

        for (entity, mut triangle) in &mut query_triangles {
            let player_point =
                Vec2::new(tranform_player.translation.x, tranform_player.translation.y);
            let seeker_point = Vec2::new(triangle.translation.x, triangle.translation.y);

            let mut directional_vector = player_point - seeker_point;
            directional_vector = directional_vector.normalize();

            triangle.translation.x += 2.0 * directional_vector.x;
            triangle.translation.y += 2.0 * directional_vector.y;

            let angle = directional_vector.y.atan2(directional_vector.x);
            triangle.rotation = Quat::from_rotation_z(angle + PI / 6.);

            if triangle.translation.x < min_x
                || triangle.translation.x > max_x
                || triangle.translation.y < min_y
                || triangle.translation.y > max_y
            {
                commands.entity(entity).despawn();
            }
        }
    }
}

fn advance_timer(
    time: Res<Time>,
    mut timer: ResMut<SpawnTimer>,
    mut move_timer: ResMut<CircleMoveTimer>,
    mut triangle_spawn: ResMut<TriangleSpawnTimer>,
) {
    timer.0.tick(time.delta());
    move_timer.0.tick(time.delta());
    triangle_spawn.0.tick(time.delta());
}

fn spawn_circles(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    timer: Res<SpawnTimer>,
    camera: Query<&OrthographicProjection>,
) {
    let camera = camera.single();
    let x_pos = camera.area.max.x;

    if timer.0.just_finished() {
        let y_pos = rand::thread_rng().gen_range(-400.0..400.0);

        commands.spawn((
            Circle,
            MaterialMesh2dBundle {
                mesh: meshes.add(shape::Circle::new(30.0).into()).into(),
                material: materials.add(Color::GREEN.into()).into(),
                transform: Transform::from_xyz(x_pos, y_pos, 1.),
                ..default()
            },
        ));
    }
}

fn spawn_triangles(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    timer: Res<TriangleSpawnTimer>,
    camera: Query<&OrthographicProjection>,
) {
    if timer.0.just_finished() {
        let camera = camera.single();
        let x_pos = camera.area.max.x - 50.;
        let y_pos = rand::thread_rng().gen_range(-360.0..360.0);
        let mut triangle_transform = Transform::from_xyz(x_pos, y_pos, 2.);
        triangle_transform.rotate_z(-PI / 6.);
        commands.spawn((
            Triangle,
            MaterialMesh2dBundle {
                mesh: meshes.add(shape::RegularPolygon::new(34., 3).into()).into(),
                material: materials.add(Color::RED.into()).into(),
                transform: triangle_transform,
                ..default()
            },
        ));
    }
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

// fn rotate_rectangle(time: Res<Time>, mut query: Query<&mut Transform, With<MovableObject>>) {
//     let mut trans = query.single_mut();
//     trans.rotate(Quat::from_rotation_z(time.delta_seconds() * PI / 2.));
// }
